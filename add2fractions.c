#include <stdio.h>
int input()
{
    int i1;
    scanf("%d",&i1);
    return i1;
}
void add(int a,int b,int c,int d,int x,int y,int gcd)
{
    int i;
    x=(a*d)+(b*c);
    y=b*d;
    for(i=1; i <= x && i <= y; ++i)
    {
        if(x%i==0 && y%i==0)
            gcd = i;
    }
    output(x,y,gcd);
}
void output(int x,int y,int gcd)
{
    printf("The added fraction is %d/%d  \n",x/gcd,y/gcd);
}
int main()
{
    int a,b,c,d,x=0,y=0,gcd=0;
    printf("Enter the numerator for 1st number :");
    a=input();
    printf("Enter the denominator for 1st number :");
    b=input();
    printf("Enter the numerator for 2nd number :");
    c=input();
    printf("Enter the denominator for 2nd number :");
    d=input();
    add(a,b,c,d,x,y,gcd);
    return 0;
}
