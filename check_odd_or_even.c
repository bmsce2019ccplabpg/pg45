#include <stdio.h>
int input();
int check(int n);
void output(int a);
int main()
{
    int n,a;
    n=input();
    a=check(n);
    output(a);
    return 0;
}
int input()
{
    int x;
    printf("Enter the number:");
    scanf("%d",&x);
    return x;
}
int check(int n)
{
    int r;
    r=n/2;
    return r;
}
void output(int a)
{
    if(a==1)
    {
        printf("The number is odd\n");
    }
    else 
    {
        printf("The number is even\n");
    }
}