#include<stdio.h>
void input(int *,int *);
void swap(int *,int*);
void output(int*,int*);
int main()
{
    int a,b;
    input(&a,&b);
    swap(&a,&b);
    output(&a,&b);
    return 0;
}
void input(int *a,int *b)
{
    int x,y;
    printf("Enter the  value of a=");
    scanf("%d",&x);
    printf("Enter the  value of b=");
    scanf("%d",&y);
    *a=x;
    *b=y;
}
void swap(int *a,int *b)
{
    int c;
    c=*a;
    *a=*b;
    *b=c;
}
void output(int *a,int *b)
{
    printf("The values of a and b after swapping is\n ");
    printf("a=%d\t b=%d",*a,*b);
}
