#include <stdio.h>
int input();
int palindrome(int);
void output(int);
int main()
{
    int n,p;
    n=input();
    p=palindrome(n);
    output(p);
    return 0;
}
int input()
{
    int x;
    printf("enter the number\n");
    scanf("%d",&x);
    return x;
}
int palindrome(int n)
{
    int n1=0,rev=0,rem=0;
    n1=n;
    while(n!=0)
    {
        rem=n%10;
        rev=(rev*10)+rem;
        n=n/10;
    }
    if(n1==rev)
        {
            return 1;
        }
        else
        {
            return 0;
        }

    }
void output(int p)
{
    if (p==1)
    {
        printf("Its a palindrome\n");
    }
    else
    {
        printf("Its not a palindrome\n");
    }
}