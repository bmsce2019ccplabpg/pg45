#include <stdio.h>
int input()
{
    int x,y;
    printf("Enter a number:");
    scanf("%d",&x);
    y=-x;
    if(x>y)
        return x;
    else
        return y;
}
int add(int a,int b)
{
    return a+b;
}
void output(int c)
{
    printf("The sum is %d\n",c);
}
int main()
{
    int a,b,c;
    a=input();
    b=input();
    c=add(a,b);
    output(c);
    return 0;
}