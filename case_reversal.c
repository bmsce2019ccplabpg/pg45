#include <stdio.h>
void input(char[]);
void length(char[]);
void output(char[]);
int main()
{
    int a;
    char s[1000];
    input(s);
    length(s);
    output(s);
    return 0;
}
void input(char s[])
{
    printf("Enter the string\n");
    scanf("%s",s);
}
void length(char s[])
{
    int i=0;
    while(s[i]!='\0')
    {
        if(s[i]<='Z' && s[i]>='A')
        {
            s[i]=s[i]+32;
        }
        else if(s[i]<='z' && s[i]>='a')
        {
            s[i]=s[i]-32;
        }
        i++;
    }
}
void output(char s[])
{
    printf("The new string is %s",s);
}