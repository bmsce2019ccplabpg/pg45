#include <stdio.h>
int main()
{
    struct COMPLEX
    {
        float r,i;
    }a,b,c;
    printf("Enter the real and complex part of first complex number respectively\n"); 
    scanf("%f %f",&a.i,&a.r);
    printf("Enter the real and complex part of second complex number respectively\n");
    scanf("%f %f",&b.i,&b.r);
    c.r=a.r+b.r;
    c.i=a.i+b.i;
    printf("The sum of 2 complex numbers is %f+i%f",c.i,c.r);
    return 0;
}