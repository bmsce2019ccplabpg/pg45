#include <stdio.h>
void input(char[]);
int length(char[]);
void output(int);
int main()
{
    int a;
    char s[1000];
    input(s);
    a=length(s);
    output(a);
    return 0;
}
void input(char s[])
{
    printf("Enter the string\n");
    scanf("%s",s);
}
int length(char s[])
{
    int i=0;
    while(s[i]!='\0')
    {
        i++;
    }
    return i;
}
void output(int a)
{
    printf("The length of string is %d",a);
}