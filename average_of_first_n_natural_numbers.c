#include <stdio.h>
int input();
float avg(int);
void output(float);
int main()
{
    int n;
    float s=0.0;
    n=input();
    s=avg(n);
    output(s);
    return 0;
}
int input()
{
    int y;
    printf("Enter the number upto which the average is required \n");
    scanf("%d",&y);
    return y;
}
float avg(int n)
{
    int s1=0;
    float a;
    for(int i=1;i<=n;i++)
    {
        s1=s1+i;
    }
    a=(float)s1/n;
    return a;
}
void output(float s)
{
    printf("the average of the number is %f\n",s);
}
