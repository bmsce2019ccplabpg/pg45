#include <stdio.h>
int main()
{
	int a,b,c,great,small,total;
	float average;
	char choice;
	printf("Enter values of a,b,c\n");
	scanf("%d %d %d",&a,&b,&c);
	printf("Enter the choice\nchoice1 to read 3 numbers\nchoice2 to get sum of 3 numbers\nchoice3 to get average of 3 numbers\nchoice4 to get greatest of 3 numbers\nchoice5 to get smallest of 3 numbers\n");
	scanf("%d",&choice);
	total=(a+b+c);
	average=(a+b+c)/3;
	switch (choice)
	{
		case 1:
			printf("a=%d,b=%d,c=%d\n",a,b,c);
			break;
		case 2:
		    printf("Sum of 3 numbers is %d\n",total);
		    break;
		case 3:
			printf("average of 3 numbers is %f\n",average);
			break;
		case 4:
		   great=(a>b)?((a>c)?a:c):((b>c)?b:c);
		   printf("greatest of 3 numbers is %d\n",great);
		   break;
		case 5:
		   small=(a<b)?((a<c)?a:c):((b<c)?b:c);
		   printf("smallest of 3 numbers is %d\n",small);
		   break;
		default:
			printf("please enter a valid choice\n");
			break;
	}
	return 0;
}