#include<stdio.h>
#include<math.h>
int main()
{
    float a,b,c,D,root1,root2,x,ii,ir;
    int n;
    printf("Enter the value of coefficient of x^2\n");
    scanf("%f",&a);
    printf("Enter the value of coefficient of x\n");
    scanf("%f",&b);
    printf("Enter the value of constant\n");
    scanf("%f",&c);
    printf("The Quadratic equation is %fx^2+(%fx)+(%f)\n",a,b,c);
    D=b*b-4*a*c;
    root1=(-b+sqrt(b*b-4*a*c))/2*a;
    root2=(-b-sqrt(b*b-4*a*c))/2*a;
    ir=(-b)/(2*a);
    ii=(sqrt(4*a*c-b*b))/2*a;
    x=-b/2*a;
    if (a!=0)
    {
    if (D>0)
        n=1;
    else if (D==0)
        n=0;
    else
        n=-1;
    switch (n)
    {
        case 1:
        printf("roots are real and distinct\n");
        printf("roots are %f %f\n",root1,root2);
        case 0:
        printf("roots are real and equal\n");
        printf("root is %f\n",x);
        default:
        printf("roots are imaginary\n");
        printf("roots are %f+i%f and %f-i%f",ir,ii,ir,ii);
    }
    }
    else
    {
        printf("given equation is not a quadratic equation");
    }
return 0;
}