#include <stdio.h>
#include <math.h>
float input();
float distance(float x1,float y1,float x2,float y2);
void output(float d);
int main()
{
    float x1,y1,x2,y2,d;
    printf("Enter the value of x1\n");
    x1=input();
    printf("Enter the value of y1\n");
    y1=input();
    printf("Enter the value of x2\n");
    x2=input();
    printf("Enter the value of y2\n");
    y2=input();
    d=distance(x1,y1,x2,y2);
    output(d);
    return 0;
}
float input()
{
    float f;
    scanf("%f",&f);
    return f;
}
float distance(float x1,float y1,float x2,float y2)
{
     float d1=0.0;
     d1=sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
     return d1;
}
void output(float d)
{
    printf("The distance is %f",d);
}