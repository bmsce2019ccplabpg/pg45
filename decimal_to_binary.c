#include <stdio.h>
int input();
int compute(int n,int a[]);
void output(int s,int a[s]);
int main()
{
    int n,a[1000],s;
    n=input();
    s=compute(n,a);
    output(s,a);
    return 0;
}
int input()
{
    int x;
    printf("Enter the number:");
    scanf("%d",&x);
    return x;
}
int compute(int n,int a[])
{
    int i=0;
    for(i=0;n>0;i++)
    {
        a[i]=n%2;
        n=n/2;
    }
    return i;
}
void output(int s,int a[s])
{
    printf("The binary of the given number is:");
    for(s=s-1;s>=0;s--)
    {
        printf("%d",a[s]);
    }
}