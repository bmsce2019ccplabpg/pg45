#include <stdio.h>
int input(int[]);
int insert(int[],int);
void output(int[],int);
int main()
{
    int n=1;
    int a[n];
    n=input(a);
    n=insert(a,n);
    output(a,n);
    return 0;
}
int input(int a[])
{
    int n;
    printf("Enter the value of n: ");
    scanf("%d",&n);
    int num;
    for(int i=0;i<n;i++)
    {
        printf("Enter a number: ");
        scanf("%d",&num);
        a[i]=num;
    }
    return n;
}
int insert(int a[],int n)
{
    int v,p;
    printf("Enter the element to be inserted and the position respectively: ");
    scanf("%d%d",&v,&p);
    for(int i=n-1;i>=p-1;i--)
    {
        a[i+1]=a[i];
    }
    a[p-1]=v;
    n++;
    return n;
}
void output(int a[],int n)
{
     printf("The changed elements of array are:\n");
     for(int i=0;i<n;i++)
     {
         printf("%d\n",a[i]);
     }  
}
