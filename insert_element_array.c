#include <stdio.h>
int input1(int []);
void input2(int *a,int *p);
void compute(int n,int h[100],int a,int p);
void output(int n,int h[100]);
int main()
{
    int h[100],n,a,p; 
    n=input1(h);
    input2(&a,&p);
    compute(n,h,a,p);
    output(n,h);
    return 0;
}
int input1(int h[100])
{
    int x,i;
    printf("Enter the size of array:");
    scanf("%d",&x);
    printf("Enter the array\n");
    for(i=0;i<x;i++)
    {
        scanf("%d",&h[i]);
    }
    return x;
}
void input2(int *a,int *p)
{
    printf("Enter the number to be inserted :");
    scanf("%d",a);
    printf("Enter the position to be inserted :");
    scanf("%d",p);
}
void compute(int n,int h[100],int a,int p)
{
    int i;
    for(i=n-1;i>=p-1;i--)
    {
        h[i+1]=h[i];
    }
    h[p-1]=a;
}
void output(int n,int h[100])
{
    int i;
    printf("The new array is\n");
    for(i=0;i<=n;i++)
    {
        printf("%d\n",h[i]);
    }
}
