#include <stdio.h>
int input();
int square(int n);
void output(int a);
int main()
{
    int n,a;
    n=input();
    a=square(n);
    output(a);
    return 0;
}
int input()
{
    int x;
    printf("Enter the number of even numbers sum you want\n");
    scanf("%d",&x);
    return x;
}
int square(int n)
{
    int s=0;
    for (int i=2;i<=(n*2);i+=2)
    { 
      s=s+(i*i);
    }
    return s;
}
void output(int a)
{
    printf("Sum of square of  even numbers is %d\n",a);
}