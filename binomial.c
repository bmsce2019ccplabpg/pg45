#include <stdio.h>
int fact(int f)
{
    int i=1,c=1;
    if(f==0)
        return 1;
    else
       while(i!=f)
        {
            c=c*i;
            i++;
        }
        return c;
}
int input()
{
    int a1;
    printf("Enter a number:");
    scanf("%d",&a1);
    return a1;
}
void comb(int a,int b[1000])
{
    int i,l,m,n,f;
    for(i=0;i<=a;i++)
    {
        f=a;
        l=fact(f);
        f=i;
        m=fact(f);
        f=a-i;
        n=fact(f);
        b[i]=l/(m*n);
    }
}
void output(int a,int b[1000])
{
    int i;
    printf("The resulting binomial series is \n");
    for(i=0;i<=a;i++)
    {
        printf("%d\t",b[i]);
    }
}
int main()
{
    int a,b[1000];
    a=input();
    comb(a,b);
    output(a,b);
    return 0;
}
